import axios from 'axios';

const BASE_URL = 'https://pokeapi.co/api/v2';

const buildPokemonData = data => ({
  id: data.id,
  name: data.name,
  image: data.sprites.front_default,
  types: data.types.map(t => t.type.name),
  abilities: data.abilities.map(a => a.ability.name),
  stats: data.stats.map(s => ({ name: s.stat.name, value: s.base_stat })),
});

class Api {
  static findPokemon(query) {
    const requestURL = `${BASE_URL}/pokemon/${query.toLowerCase()}/`;
    return axios
      .get(requestURL)
      .then(response => buildPokemonData(response.data))
      .catch(err => {
        throw err;
      });
  }
}

export default Api;
