import { createSelector } from 'reselect';
import { getPokemonByName } from 'containers/Pokedex/selectors';
import { getById } from 'containers/Lineup/selectors';
import { initialState } from './reducer';

const getEditor = state => state.get('editor', initialState);
const getActiveMemberId = state => getEditor(state).get('activeMemberId');

const getMember = createSelector(
  [getById, getActiveMemberId],
  (byId, memberId) => {
    const member = byId.get(memberId);
    return member && member.toJS();
  },
);

const getParentPokemon = createSelector(
  [getPokemonByName, getMember],
  (byName, member) => {
    if (member) {
      return byName.get(member.parentPokemonName).toJS();
    }
    return null;
  },
);

export { getMember, getParentPokemon };
