import { mapDispatchToProps } from '../index';
import { updateName, remove } from '../actions';

describe('<Editor />', () => {
  describe('mapDispatchToProps', () => {
    const dispatchSpy = jest.fn();
    const props = mapDispatchToProps(dispatchSpy);

    describe('onNameChange', () => {
      it('dispatches the updateName action', () => {
        const id = '1234';
        const value = 'greninja';
        props.onNameChange(id, value);
        expect(dispatchSpy).toHaveBeenCalledWith(updateName(id, value));
      });
    });

    describe('onRemove', () => {
      it('dispatches the remove action', () => {
        const id = '1234';
        props.onRemove(id);
        expect(dispatchSpy).toHaveBeenCalledWith(remove(id));
      });
    });
  });
});
