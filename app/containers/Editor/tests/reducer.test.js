import { fromJS } from 'immutable';
import { viewMember } from 'containers/Lineup/actions';
import editorReducer, { initialState } from '../reducer';

describe('editorReducer', () => {
  let state;

  beforeEach(() => {
    state = fromJS({});
  });

  it('returns the initial state', () => {
    expect(editorReducer(undefined, {})).toEqual(initialState);
  });

  it('handles VIEW_MEMBER', () => {
    expect(editorReducer(state, viewMember('12341'))).toMatchSnapshot();
  });
});
