import { UPDATE_NAME, REMOVE } from '../constants';
import { updateName, remove } from '../actions';

describe('Editor actions', () => {
  describe('updateName', () => {
    let action;

    const id = '123';
    const value = 'pichu';

    beforeEach(() => {
      action = updateName(id, value);
    });

    it('has a type of UPDATE_NAME', () => {
      expect(action.type).toEqual(UPDATE_NAME);
    });

    it('has the payload', () => {
      expect(action.payload).toEqual({ id, value });
    });
  });

  describe('remove', () => {
    let action;

    const id = '123';

    beforeEach(() => {
      action = remove(id);
    });

    it('has a type of REMOVE', () => {
      expect(action.type).toEqual(REMOVE);
    });

    it('has the payload', () => {
      expect(action.payload).toEqual({ id });
    });
  });
});
