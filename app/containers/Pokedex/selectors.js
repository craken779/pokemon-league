import { createSelector } from 'reselect';
import { initialState } from './reducer';

const getPokedex = state => state.get('pokedex', initialState);
const selectIsSearching = state => getPokedex(state).get('isSearching');
const selectNotFound = state => getPokedex(state).get('notFound');
const getPokemonByName = state =>
  getPokedex(state).getIn(['pokemon', 'byName']);
const getQuery = state => getPokedex(state).get('query');
const getActivePokemonName = state =>
  getPokedex(state).get('activePokemonName');

const findPokemonFromQuery = createSelector(
  [getPokemonByName, getQuery],
  (byName, query) => {
    const pokemon = byName.get(query.toLowerCase());
    return pokemon && pokemon.toJS();
  },
);

const activePokemon = createSelector(
  [getPokemonByName, getActivePokemonName],
  (byName, activePokemonName) => {
    const pokemon = byName.get(activePokemonName);
    return pokemon && pokemon.toJS();
  },
);

export {
  getPokedex,
  selectIsSearching,
  selectNotFound,
  getQuery,
  getPokemonByName,
  getActivePokemonName,
  findPokemonFromQuery,
  activePokemon,
};
