import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Box } from '@rebass/grid';
import Img from 'react-image';

import Ghost from 'images/ghost-missingno.png';
import Pokemon from './Pokemon';

const Wrapper = styled(Box).attrs({
  p: 3,
})`
  background: ${props => props.theme.coldAsh};
  border: 4px solid ${props => props.theme.darkrai};
  border-radius: 8px;
  min-width: 100%;
`;

const InfoText = styled.span`
  display: block;
  font-size: 24px;
`;

const SearchFailedWrapper = styled.div`
  text-align: center;
`;

const GhostWrapper = styled.div`
  display: inline-block;
  max-width: 400px;
`;

const GhostImage = styled(Img)`
  display: block;
  width: 100%;
`;

/* eslint-disable react/prefer-stateless-function */
class PokemonData extends React.PureComponent {
  renderSearching() {
    return <InfoText>Searching for Pokemon...</InfoText>;
  }

  renderFailedSearch() {
    return (
      <SearchFailedWrapper>
        <GhostWrapper>
          <GhostImage src={Ghost} />
        </GhostWrapper>
        <InfoText>Search failed :(</InfoText>
      </SearchFailedWrapper>
    );
  }

  renderPokemon(data) {
    const { isAddDisabled, onAddClick } = this.props;
    return (
      <Pokemon
        {...data}
        isAddDisabled={isAddDisabled}
        onAddClick={onAddClick}
      />
    );
  }

  render() {
    const { pokemon, notFound, isSearching } = this.props;

    return (
      <Wrapper>
        {pokemon && this.renderPokemon(pokemon)}
        {notFound && this.renderFailedSearch()}
        {isSearching && this.renderSearching()}
      </Wrapper>
    );
  }
}

PokemonData.propTypes = {
  isSearching: PropTypes.bool,
  isAddDisabled: PropTypes.bool,
  notFound: PropTypes.bool,
  pokemon: PropTypes.object,
  onAddClick: PropTypes.func.isRequired,
};

export default PokemonData;
