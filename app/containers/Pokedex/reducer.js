import { fromJS } from 'immutable';
import {
  SEARCH_POKEMON,
  SEARCH_SERVICE,
  POKEMON_FOUND,
  POKEMON_FOUND_FROM_CACHE,
  POKEMON_NOT_FOUND,
} from './constants';

export const initialState = fromJS({
  isSearching: false,
  notFound: false,
  activePokemonName: null,
  query: null,
  pokemon: {
    byName: {},
  },
});

function pokedexReducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_POKEMON:
      return state.merge({
        isSearching: false,
        notFound: false,
        activePokemonName: null,
        query: action.payload.query,
      });
    case SEARCH_SERVICE:
      return state.merge({
        isSearching: true,
        notFound: false,
      });
    case POKEMON_FOUND_FROM_CACHE:
      return state.merge({
        isSearching: false,
        notFound: false,
        activePokemonName: action.payload.pokemon.name,
      });
    case POKEMON_FOUND:
      return state.mergeDeep({
        isSearching: false,
        notFound: false,
        activePokemonName: action.payload.pokemon.name,
        pokemon: {
          byName: {
            [action.payload.pokemon.name]: action.payload.pokemon,
          },
        },
      });
    case POKEMON_NOT_FOUND:
      return state.merge({
        isSearching: false,
        notFound: true,
        activePokemonName: null,
      });
    default:
      return state;
  }
}

export default pokedexReducer;
