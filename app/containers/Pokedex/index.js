import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { createStructuredSelector } from 'reselect';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { isLineupFull } from 'containers/Lineup/selectors';
import { selectIsSearching, selectNotFound, activePokemon } from './selectors';
import reducer from './reducer';
import saga from './saga';
import SearchForm from './SearchForm';
import PokemonData from './PokemonData';
import { searchPokemon, addToLineup } from './actions';

const Wrapper = styled(Flex)`
  flex-direction: column;
  min-height: 100%;
`;

const PokemonDataWrapper = styled(Flex)`
  align-items: stretch;
  flex: 1 0 auto;
`;

export function Pokedex({
  onSearch,
  onAddClick,
  isSearching,
  isAddDisabled,
  notFound,
  pokemon,
}) {
  return (
    <Wrapper>
      <Box pb={3}>
        <SearchForm onSearch={onSearch} />
      </Box>
      <PokemonDataWrapper>
        <PokemonData
          pokemon={pokemon}
          notFound={notFound}
          isSearching={isSearching}
          isAddDisabled={isAddDisabled}
          onAddClick={onAddClick}
        />
      </PokemonDataWrapper>
    </Wrapper>
  );
}

Pokedex.propTypes = {
  onSearch: PropTypes.func.isRequired,
  onAddClick: PropTypes.func.isRequired,
  isSearching: PropTypes.bool.isRequired,
  isAddDisabled: PropTypes.bool.isRequired,
  notFound: PropTypes.bool.isRequired,
  pokemon: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  isSearching: selectIsSearching,
  isAddDisabled: isLineupFull,
  notFound: selectNotFound,
  pokemon: activePokemon,
});

export const mapDispatchToProps = dispatch => ({
  onSearch: query => {
    dispatch(searchPokemon(query));
  },
  onAddClick: name => {
    dispatch(addToLineup(name));
  },
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'pokedex', reducer });
const withSaga = injectSaga({ key: 'pokedex', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Pokedex);
