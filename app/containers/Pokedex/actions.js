import {
  SEARCH_POKEMON,
  SEARCH_SERVICE,
  POKEMON_FOUND,
  POKEMON_FOUND_FROM_CACHE,
  POKEMON_NOT_FOUND,
  ADD_TO_LINEUP,
} from './constants';

const searchPokemon = query => ({
  type: SEARCH_POKEMON,
  payload: {
    query,
  },
});

const searchService = () => ({
  type: SEARCH_SERVICE,
});

const pokemonFound = pokemon => ({
  type: POKEMON_FOUND,
  payload: {
    pokemon,
  },
});

const pokemonFoundFromCache = pokemon => ({
  type: POKEMON_FOUND_FROM_CACHE,
  payload: {
    pokemon,
  },
});

const pokemonNotFound = error => ({
  type: POKEMON_NOT_FOUND,
  payload: {
    error,
  },
});

const addToLineup = name => ({
  type: ADD_TO_LINEUP,
  payload: {
    name,
  },
});

export {
  searchPokemon,
  searchService,
  pokemonFound,
  pokemonFoundFromCache,
  pokemonNotFound,
  addToLineup,
};
