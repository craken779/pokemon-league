import React from 'react';
import { shallow } from 'enzyme';

import SearchForm from '../SearchForm';

describe('<SearchForm />', () => {
  describe('on form submit', () => {
    let renderedComponent;

    const onSearchSpy = jest.fn();
    const query = 'mew';

    beforeEach(() => {
      renderedComponent = shallow(<SearchForm onSearch={onSearchSpy} />);
      renderedComponent.find('form').simulate('submit', {
        preventDefault: jest.fn(),
        target: { query: { value: query } },
      });
    });

    it('calls the given onSearch callback with the query', () => {
      expect(onSearchSpy).toHaveBeenCalledWith(query);
    });
  });
});
