import {
  searchPokemon,
  searchService,
  pokemonFound,
  pokemonFoundFromCache,
  pokemonNotFound,
  addToLineup,
} from '../actions';
import {
  SEARCH_POKEMON,
  SEARCH_SERVICE,
  POKEMON_FOUND,
  POKEMON_FOUND_FROM_CACHE,
  POKEMON_NOT_FOUND,
  ADD_TO_LINEUP,
} from '../constants';

describe('Pokedex actions', () => {
  describe('searchPokemon', () => {
    let action;

    const query = 'pichu';

    beforeEach(() => {
      action = searchPokemon(query);
    });

    it('has a type of SEARCH_POKEMON', () => {
      expect(action.type).toEqual(SEARCH_POKEMON);
    });

    it('has the payload', () => {
      expect(action.payload).toEqual({ query });
    });
  });

  describe('searchService', () => {
    let action;

    beforeEach(() => {
      action = searchService();
    });

    it('has a type of SEARCH_SERVICE', () => {
      expect(action.type).toEqual(SEARCH_SERVICE);
    });
  });

  describe('pokemonFound', () => {
    let action;

    const pokemon = { foo: 1 };

    beforeEach(() => {
      action = pokemonFound(pokemon);
    });

    it('has a type of POKEMON_FOUND', () => {
      expect(action.type).toEqual(POKEMON_FOUND);
    });

    it('has the payload', () => {
      expect(action.payload).toEqual({ pokemon });
    });
  });

  describe('pokemonFoundFromCache', () => {
    let action;

    const pokemon = { foo: 1 };

    beforeEach(() => {
      action = pokemonFoundFromCache(pokemon);
    });

    it('has a type of POKEMON_FOUND_FROM_CACHE', () => {
      expect(action.type).toEqual(POKEMON_FOUND_FROM_CACHE);
    });

    it('has the payload', () => {
      expect(action.payload).toEqual({ pokemon });
    });
  });

  describe('pokemonNotFound', () => {
    let action;

    const error = new Error('Failed.');

    beforeEach(() => {
      action = pokemonNotFound(error);
    });

    it('has a type of POKEMON_NOT_FOUND', () => {
      expect(action.type).toEqual(POKEMON_NOT_FOUND);
    });

    it('has the payload', () => {
      expect(action.payload).toEqual({ error });
    });
  });

  describe('addToLineup', () => {
    let action;

    const name = 'pichu';

    beforeEach(() => {
      action = addToLineup(name);
    });

    it('has a type of ADD_TO_LINEUP', () => {
      expect(action.type).toEqual(ADD_TO_LINEUP);
    });

    it('has the payload', () => {
      expect(action.payload).toEqual({ name });
    });
  });
});
