import React from 'react';
import styled from 'styled-components';
import { Flex, Box } from '@rebass/grid';

import Pokedex from 'containers/Pokedex';
import Lineup from 'containers/Lineup';
import Editor from 'containers/Editor';

const Wrapper = styled(Flex)`
  align-items: stretch;
  min-height: 100%;
  min-width: 100%;
`;

const LeftPaneWrapper = styled(Flex)`
  flex-direction: column;
  min-height: 100%;
  min-width: 100%;
`;

/* eslint-disable react/prefer-stateless-function */
export default class HomePage extends React.PureComponent {
  render() {
    return (
      <Wrapper>
        <Box width={3 / 5} px={2}>
          <LeftPaneWrapper>
            <Box pb={3}>
              <Lineup />
            </Box>
            <Flex flex="1 0 auto" alignItems="stretch">
              <Editor />
            </Flex>
          </LeftPaneWrapper>
        </Box>
        <Box width={2 / 5} px={2}>
          <Pokedex />
        </Box>
      </Wrapper>
    );
  }
}
