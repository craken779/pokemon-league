import { fromJS } from 'immutable';
import cuid from 'cuid';
import { UPDATE_NAME, REMOVE } from 'containers/Editor/constants';
import { ADD_TO_LINEUP } from 'containers/Pokedex/constants';

export const initialState = fromJS({
  pokemon: {
    byId: {},
    allIds: [],
  },
});

function lineupReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_TO_LINEUP: {
      const newId = cuid();
      const parentPokemonName = action.payload.name;
      return state
        .mergeDeep({
          pokemon: {
            byId: {
              [newId]: {
                id: newId,
                name: parentPokemonName,
                parentPokemonName,
              },
            },
          },
        })
        .updateIn(['pokemon', 'allIds'], allIds => allIds.push(newId));
    }
    case UPDATE_NAME:
      return state.mergeDeep({
        pokemon: {
          byId: {
            [action.payload.id]: {
              name: action.payload.value,
            },
          },
        },
      });
    case REMOVE: {
      const index = state
        .getIn(['pokemon', 'allIds'])
        .findIndex(id => id === action.payload.id);
      return state
        .updateIn(['pokemon', 'allIds'], allIds => allIds.delete(index))
        .updateIn(['pokemon', 'byId'], byId => byId.delete(action.payload.id));
    }
    default:
      return state;
  }
}

export default lineupReducer;
