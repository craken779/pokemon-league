import lineupReducer, { initialState } from '../reducer';

describe('lineupReducer', () => {
  it('returns the initial state', () => {
    expect(lineupReducer(undefined, {})).toEqual(initialState);
  });
});
