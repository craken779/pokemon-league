import React from 'react';
import { shallow } from 'enzyme';
import Img from 'react-image';

import { Lineup, mapDispatchToProps } from '../index';
import { viewMember } from '../actions';

describe('<Lineup />', () => {
  describe('Component', () => {
    let renderedComponent;

    const members = [
      {
        member: { foo: 1 },
        parentPokemon: { image: 'some-image.png' },
      },
    ];

    beforeEach(() => {
      renderedComponent = shallow(
        <Lineup onMemberSelect={jest.fn()} members={members} />,
      );
    });

    it('renders the members', () => {
      expect(renderedComponent.contains(<Img src="some-image.png" />)).toEqual(
        true,
      );
    });
  });

  describe('mapDispatchToProps', () => {
    const dispatchSpy = jest.fn();
    const props = mapDispatchToProps(dispatchSpy);

    describe('onMemberSelect', () => {
      it('dispatches the viewMember action', () => {
        const index = 1;
        props.onMemberSelect(index);
        expect(dispatchSpy).toHaveBeenCalledWith(viewMember(index));
      });
    });
  });
});
