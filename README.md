# Pokemon League

To become the very best, you must prepare your line up first!

## How it works

Just search for a Pokemon by its exact name. The API we are fetching data is from https://pokeapi.co/.

Then click on the "Add To Lineup" button. The pokemon should show up on the lineup list. There's a limit of 6 members
so you'll have to remove some if you want to change and add more.

For now, only the pokemon's name is editable.

## Running locally

This was tested on Node v10.10.0 but should at least work with v8+ given it was built on top of [react boilerplate](https://github.com/react-boilerplate/react-boilerplate).

Just run `yarn install` and then `yarn start`. Test the app on http://localhost:3000.

## Demo

https://pokemon-league.herokuapp.com/
